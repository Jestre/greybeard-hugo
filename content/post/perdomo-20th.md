+++
author = ""
comments = false	# set false to hide Disqus
date = "2017-09-09T11:20:40-04:00"
draft = false
image = ""
menu = ""		# set "main" to add this content to the main menu
share = true	# set false to hide share buttons
slug = "perdomo-20th"
tags = ["life","cigars"]
title = "Perdomo 20th Anniversary"
+++

Just finished a lovely Perdomo 20th Anniversary cigar.  While this one didn’t burn as evenly as many of the others I have had, it was still a great smoke.  And with the weather being on the cooler side, it was a pleasant way to spend a cool Saturday evening on the front porch by the light of a small Tiki “fireplace”.  It is really like an oversized candle, but it lends a great deal of ambience to the scene, and provides just enough light so as to prevent one from sitting in the dark.

Watched a few Howard Stern “It’s So Wrong” videos.  It is amazing what people will do for a buck :)

And now, time for a bit of television.
