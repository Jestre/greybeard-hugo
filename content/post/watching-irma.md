+++
author = ""
comments = false	# set false to hide Disqus
date = "2017-09-04T11:04:04-04:00"
draft = false
image = ""
menu = ""		# set "main" to add this content to the main menu
share = true	# set false to hide share buttons
slug = "watching-irma"
tags = ["life","weather"]
title = "Watching Irma"
+++

Hurricane Irma continues its steady westward march across the Atlantic Ocean towards the US.  And since early (and some current) projections have it coming ashore here in NC, we are all watching closely the progress of this storm.

In one bit of seemingly good news, a high pressure system to the North of Irma has keep it down and moving more westerly than northwesterly, which means that it may make landfall in Florida instead of here in NC.  Hopefully that trend will continue.

![Irma Track](/images/storm_11-201709041055.gif)