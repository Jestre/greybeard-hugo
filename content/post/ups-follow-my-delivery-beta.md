+++
author = ""
comments = false	# set false to hide Disqus
date = "2017-11-02T16:59:40-04:00"
draft = false
image = "/images/posts/ups-screenshot.jpg"
menu = ""		# set "main" to add this content to the main menu
share = true	# set false to hide share buttons
slug = "ups-follow-my-delivery-beta"
tags = ["life","tech"]
title = "UPS Follow My Delivery Beta"
+++

I took the day off today since I saw that my new iPhone X delivery would require a signature, and this morning I received an email from [UPS](https://ups.com) with a link to watch the location of the truck carrying my phone on a live map.  Great idea, I thought, so I clicked on the link, and was taken to a map which showed the current location of the truck.  It was several miles away, but since it was most likely coming out of Raleigh, there was nothing unusual about that, and the website indicated that my phone would be delivered by 3:00pm, so no problem.  

But that was at 8:47am.  It is now ~~4:49pm~~ ~~5:49pm~~ ~~6:49pm~~ 7:49 and I have been watching the truck, which at one point was a few hundred yards from my house, driving farther and farther away.  Does that mean I will not be getting the phone today?  Perhaps this isn't such a good idea.  Had I not known how close it was several hours ago, I would still be sitting here patiently waiting, instead of getting more frustrated by the minute.

Ah well, such is life.

Update:  Phone finally delivered at 8:13pm - jeez!
